# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "1.3.2"
  end
end
